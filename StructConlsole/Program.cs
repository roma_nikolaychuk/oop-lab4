﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructConlsole
{
    public struct Product           //представляє інформацію про один товар, який зберігається на складі
    {
        public string Name;    //Назва товару
        public Currency Cost;     //Вартість одиниці товару(структура Currency)
        public int Quantity;    //Кількість наявних товарів на складі
        public string Producer; //Назва компанії-виробника
        public double Weight;   //Вага одиниці товару

        //Конструктор
        public Product(string name, decimal cost, int quantity, string producer, double weight)
        {
            Name = name;
            Cost = new Currency
            {
                Name = "UAH",                                //	USD - долари, UAH - гривні, EUR - євро

                ExRate = cost                                // 1 USD = 28.25,   1 EUR = 32.63
            };
            Quantity = quantity;
            Producer = producer;
            Weight = weight;
        }

        public void GetPriceInUAH(Product[] product, int n)       //Повертає ціну одиниці товару в гривнях
        {
            for (int i = 0; i < n; i++)
            {

                Console.WriteLine("Ціна \"{0}\" за одиницю товару - {1} грн", product[i].Name, product[i].Cost.ExRate);
            }
        }

        public void GetTotalPriceInUAH(Product[] product, int n)    //Повертає загальну вартість усіх наявних на складі товарів даного виду
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Загальна вартість усього товару \"{0}\" на складі даного виду - {1} грн", product[i].Name, product[i].Quantity * product[i].Cost.ExRate);
            }
        }

        public void GetTotalWeight(Product[] product, int n)        //Повертає загальну вагу усіх товарів на складі даного виду
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Загальна вага усіх товарів \"{0}\" на складі даного виду - {1} кг", product[i].Name, product[i].Quantity * product[i].Weight);
            }
            Console.WriteLine();
        }
    }

    public struct Currency
    {
        public string Name;     //Назва валюти
        public decimal ExRate;   //курс (дробове число - кількість гривень та копійок, що дають за одну одиницю валюти)
        //Конструктор
        public Currency(string name, decimal exRate)
        {
            Name = name;
            ExRate = exRate;
        }
    }

    class Program
    {
        static void ReadProductsArray()
        {
            int n;
            do
            {
                Console.Write("Введіть кількість товарів n = ");
                if ((!int.TryParse(Console.ReadLine(), out n)) || n <= 0)
                    Console.WriteLine("Помилка введеня значення n! Будь ласка введіть ціле число!");
                else
                    Console.WriteLine();
                break;
            }
            while (true);

            Product[] product = new Product[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write("Введіть ім'я товару: ");
                product[i].Name = Console.ReadLine();
                Console.Write("Введіть кількість наявних товарів на складі: ");
                product[i].Quantity = Convert.ToInt32(Console.ReadLine());
                Console.Write("Введіть назву компанії-виробника: ");
                product[i].Producer = Console.ReadLine();
                Console.Write("Введіть вартість товару (грн): ");
                product[i].Cost.ExRate = Convert.ToDecimal(Console.ReadLine());
                Console.Write("Введіть вагу товару (кг): ");
                product[i].Weight = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine();
            }

            Console.WriteLine();
            if (n == 1)
            {
                PrintProduct(product, n);

            }
            else
            {
                PrintProducts(product, n);
            }

            decimal max, min;
            string MaxName, MinName;
            GetProductsInfo(product, n, out max, out MaxName, out min, out MinName);
            Console.WriteLine("Найдорожший товар \"{0}\" - {1} грн", MaxName, max);
            Console.WriteLine("Найдешевший товар \"{0}\" - {1} грн", MinName, min);
            Console.WriteLine();
            SortProductsByPrice(product, n);
            Console.WriteLine();
            SortProductsByCount(product, n);
        }

        static void PrintProduct(Product[] product, int n)
        {
            //Вивід на екран однієї структури
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Назва товару - " + product[i].Name);
                Console.WriteLine("Кількість наявних товарів на складі - " + product[i].Quantity);
                Console.WriteLine("Назва компанії-виробника - " + product[i].Producer);
                Console.WriteLine("Вартість товару - " + product[i].Cost.ExRate + "грн");
                Console.WriteLine("Вага одиниці товару - " + product[i].Weight + "кг");
                Console.WriteLine();
            }
        }
        //Вивід на екран масиву структур
        static void PrintProducts(Product[] product, int n)
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Назва товару - " + product[i].Name);
                Console.WriteLine("Кількість наявних товарів на складі - " + product[i].Quantity);
                Console.WriteLine("Назва компанії-виробника - " + product[i].Producer);
                Console.WriteLine("Вартість товару - " + product[i].Cost.ExRate + " грн");
                Console.WriteLine("Вага одиниці товару - " + product[i].Weight + " кг");
                Console.WriteLine();
            }
            Console.WriteLine();
            for (int i = 0; i < 1; i++)
            {
                product[i].GetPriceInUAH(product, n);
                Console.WriteLine();
                product[i].GetTotalPriceInUAH(product, n);
                Console.WriteLine();
                product[i].GetTotalWeight(product, n);
            }

        }

        static void GetProductsInfo(Product[] product, int n, out decimal max, out string MaxName, out decimal min, out string MinName)
        {
            MaxName = "";
            max = product[0].Cost.ExRate;
            min = product[0].Cost.ExRate;
            MinName = "";
            for (int i = 0; i < n; i++)
            {
                if (product[i].Cost.ExRate >= max)
                {
                    max = product[i].Cost.ExRate;
                    MaxName = product[i].Name;

                }

                if (product[i].Cost.ExRate <= min)
                {
                    min = product[i].Cost.ExRate;
                    MinName = product[i].Name;
                }
            }
        }

        static void SortProductsByPrice(Product[] product, int n) //приймає масив структур типу Product і сортує його за зростанням ціни
        {
            Array.Sort(product, new Comparison<Product>((a, b) => a.Cost.ExRate.CompareTo(b.Cost.ExRate)));
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(product[i].Name + " - " + product[i].Cost.ExRate + " грн ");
            }

        }

        static void SortProductsByCount(Product[] product, int n)  // приймає масив структур типу Product і сортує його за кількістю товарів на складі
        {
            Array.Sort(product, new Comparison<Product>((a, b) => a.Quantity.CompareTo(b.Quantity)));
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(product[i].Name + " - " + product[i].Quantity + " кількість");
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            ReadProductsArray();
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}